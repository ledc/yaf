<?php
require_once __DIR__ . '/init.php';

use Yaf\Application;
use Yaf\Request\Simple;

if (PHP_SAPI !== 'cli') {
    exit("You must run the CLI environment\n");
}
// 永不超时
ini_set('max_execution_time', 0);
set_time_limit(0);

$app = new Application(APPLICATION_PATH . "/conf/application.ini");
$app->getDispatcher()->dispatch(new Simple());
