<?php

namespace Movie;

use BadRequestException;
use Selector;

/**
 * Class IMDb
 * @package Movie
 */
class IMDb extends AbstractMovie
{
    /**
     * 父类的初始化回调，子类重写此方法
     */
    public function onInit()
    {
        // 设置超时
        ini_set('max_execution_time', 120);
        set_time_limit(120);
    }

    /**
     * IMDb解码方法，抽象方法必须实现
     * @param string $tt IMDb编号
     * @return array|bool
     * @throws BadRequestException
     */
    public function SubjectPage($tt = '')
    {
        // 页面选择器
        $selector = [
            'title' => '//title',               // 网页标题栏
            'details' => '//div[@id="titleDetails"]',      // 影视信息 Details
            'year' => '//*[@id="titleYear"]//a',       // 年份
            'datetime' => '//time@datetime',    // 时长  ISO 8601(PnYnMnDTnHnMnS)
            'time' => '//time',                 // 时长  ISO 8601(1h 59min)
        ];
        // 返回的数据结构
        $rs = [
            'rating' => [
                'contentRating' => [],  // 美国内容等级 US Content Rating
                "max" => 10,
                "average" => "",    // 评分
                "numRaters" => 0,   // 评分人数
                "min" => 0
            ],
            'id' => $tt,            // 电影条目id
            'imdb' => $tt,          // 兼容豆瓣
            'alt' => '',            // 电影条目页 URL
            'year' => '',           // 年份
            'title' => '',          // 中文标题
            'original_title' => '', // 原名
            'aka' => [],            // 又名
            'images' => [],         // 电影海报图
            'pubdates' => [],       // 上映首映日期
            'mainland_pubdate' => '',   // 大陆上映日期
            'languages' => [],      // 语言
            'tags' => [],           // 标签
            'durations' => [],      // 片长
            'duration' => '',       // 片长(原值PnYnMnDTnHnMnS)
            'genres' => [],         // 影片类型
            'countries' => [],      // 制片国家/地区
            'summary' => '',        // 剧情简介
            'subtype' => '',        // 条目分类
            'directors' => [],  // 导演
            'writers' => [],    // 编剧
            'casts' => [],      // 演员
            'trailers' => [],   // 预告片
            'videos' => [],     // 在线播放
            'photos' => [],     // 电影剧照
            'mobile_url' => '', // 移动版条目页 URL
            'awards' => [],     // 获奖情况
            'JsonLD' => [],     // 页面解码特有
            'details' => [],     // 详情
            'releaseinfo' => [],   // 发行信息
            '@form' => 'imdb',
        ];
        // 数据缓存
        $cacheKey = ['Model', 'IMDb', $tt];
        if ($res = $this->getFileCache($cacheKey)) {
            return $res;
        }

        $subjectUrl = str_replace('{id}', $tt, self::IMDbUrl['subject']);
        $html = $this->request(self::IMDbBase . $subjectUrl, [], false);
        // 搜索特征码，加强检查
        if (strpos($html, '/title/' . $tt) === false) {
            $this->err_msg = '不存在的IMDb编号：' . $tt;
            throw new BadRequestException($tt . '的影视条目不存在！', 404);
        }
        /**
         * 正常流程
         */
        #\App\p($html);exit;
        // 获取网页结构化数据
        $arrayLD = $this->getJsonLD($html);
        if ($arrayLD) {
            //print_r($arrayLD); exit;
            // 结构化数据
            $rs['subtype'] = strtolower($arrayLD['@type'] ?? '');
            if (isset($arrayLD['director'])) {
                $rs['directors'] = $this->formatCelebrityInJsonLD($arrayLD['director']);
            }
            if (isset($arrayLD['creator'])) {
                $rs['writers'] = $this->formatCelebrityInJsonLD($arrayLD['creator']);
            }
            if (isset($arrayLD['actor'])) {
                $rs['casts'] = $this->formatCelebrityInJsonLD($arrayLD['actor']);
            }
            $rs['alt'] = self::IMDbBase . $arrayLD['url'];
            $rs['original_title'] = $arrayLD['name'] ?? '';
            $rs['images']['small'] = $arrayLD['image'] ?? '';
            $rs['images']['large'] = $arrayLD['image'] ?? '';
            $rs['images']['medium'] = $arrayLD['image'] ?? '';
            if (isset($arrayLD['genre'])) {
                $rs['genres'] = is_array($arrayLD['genre']) ? $arrayLD['genre'] : array($arrayLD['genre']);
            }
            // 等级、评分
            if (isset($arrayLD['contentRating']) && $arrayLD['contentRating']) {
                $rs['rating']['contentRating'] = is_array($arrayLD['contentRating']) ? $arrayLD['contentRating'] : array($arrayLD['contentRating']);
            }
            if (isset($arrayLD['aggregateRating']) && $arrayLD['aggregateRating']) {
                $rs['rating']['@type'] = $arrayLD['aggregateRating']['@type'] ?? '';
                $rs['rating']['max'] = $arrayLD['aggregateRating']['bestRating'] ?? 10;
                $rs['rating']['average'] = $arrayLD['aggregateRating']['ratingValue'] ?? 0;
                $rs['rating']['numRaters'] = $arrayLD['aggregateRating']['ratingCount'] ?? 0;
                $rs['rating']['min'] = $arrayLD['aggregateRating']['worstRating'] ?? 1;
            }
            $rs['summary'] = $arrayLD['description'] ?? '';
            if (isset($arrayLD['datePublished'])) {
                $rs['pubdates'] = is_array($arrayLD['datePublished']) ? $arrayLD['datePublished'] : array($arrayLD['datePublished']);
            }
            if (isset($arrayLD['keywords']) && $arrayLD['keywords']) {
                $rs['tags'] = strpos($arrayLD['keywords'], ',') != false ? explode(',', $arrayLD['keywords']) : array($arrayLD['keywords']);
            }
            $rs['review'] = $arrayLD['review'] ?? '';
            // 持续时间 格式：PnYnMnDTnHnMnS 例如：PT1H59M
            if (isset($arrayLD['duration']) && $arrayLD['duration']) {
                $rs['duration'] = $arrayLD['duration'];
                if (preg_match(self::PrimaryRegex['duration'], $arrayLD['duration'], $matchs)) {
                    // 转换 秒
                    $seconds = $this->ISO8601_to_seconds($arrayLD['duration']);
                    if ($seconds) {
                        $rs['durations'] = array($seconds);
                    }
                }
            }
            if (isset($arrayLD['trailer']) && $arrayLD['trailer']) {
                // $arrayLD['trailer']['@type']可选值：VideoObject |
                $rs['trailers'] = isset($arrayLD['trailer']['@type']) ? array($arrayLD['trailer']) : $arrayLD['trailer'];
            }
            $rs['JsonLD'] = $arrayLD;
        } else {
            // 结构化数据解码失败时，调用网页信息解码
        }
        // 页面信息提取
        $rs['year'] = Selector::select($html, $selector['year']);
        $title = Selector::select($html, $selector['title']);
        $rs['title'] = trim(str_replace('- IMDb', '', $title));
        // 主要信息
        $details_html = Selector::select($html, $selector['details']);
        $details = $this->SubjectDetails($details_html);
        if ($details) {
            $rs['languages'] = $details['Language'] ?? [];
            $rs['countries'] = $details['Country'] ?? [];
            $rs['details'] = $details;
        }
        $releaseinfo = $this->SubjectReleaseinfo($tt);
        if ($releaseinfo) {
            if (isset($releaseinfo['aka']) && $releaseinfo['aka']) {
                $rs['aka'] = $releaseinfo['aka'];
                unset($releaseinfo['aka']);
            }
            $rs['releaseinfo'] = $releaseinfo;
        }
        $fullcredits = $this->SubjectFullcredits($tt);
        if ($fullcredits) {
            $rs['fullcredits'] = $fullcredits;
        }
        // 获奖情况

        // 设置缓存
        $this->setFileCache($cacheKey, $rs, $this->cacheExpire * 5);        // 文件缓存35天
        return $rs;
    }

    /**
     * imdb电影条目页面，容器解码 Details
     * @desc 选择器：//div[@id="titleDetails"]
     * @param mixed $html
     * @return array
     */
    public function SubjectDetails($html = ''): array
    {
        $selector = [
            'txt-block' => '//*[@class="txt-block"]',       // 影视信息 Details内容块
            'h4' => '//h4',     // 第一列
            'a' => '//a',       // 区域、语言等
            'datetime' => '//time@datetime',    // 时长  ISO 8601(PnYnMnDTnHnMnS)
            'time' => '//time',                 // 时长  ISO 8601(1h 59min)
        ];
        $delimiter = '<hr/>';   // 边界上的分隔字符
        $temp_array = explode($delimiter, $html);
        $details = [];
        foreach ($temp_array as $k => $v) {
            $txt = Selector::select($v, $selector['txt-block']);
            if ($txt) {
                $txt = is_array($txt) ? $txt : array($txt);
                foreach ($txt as $kk => $item) {
                    $h4 = Selector::select($item, $selector['h4']);
                    $h4 = str_replace(["&nbsp;", "&amp;", "\n", "\r\n", "\r", "\0", "\t", "\x0B", ' ', " "], '', rtrim($h4, ':'));
                    $h4 = preg_replace('/\W/', '', $h4);
                    if (empty($h4)) {
                        continue;
                    }
                    switch ($h4) {
                        case 'Country':
                        case 'Language':
                        case 'SoundMix':
                        case 'Color':
                            $a_txt = Selector::select($item, $selector['a']);
                            if ($a_txt) {
                                $details[$h4] = is_array($a_txt) ? $a_txt : [$a_txt];
                            }
                            break;
                        case 'Runtime':
                            $details[$h4] = Selector::select($item, $selector['time']);
                            break;
                        case 'OfficialSites':
                        case 'ProductionCo':
                        case 'FilmingLocations':
                            // 跳过的条目
                            break;
                        default:
                            $temp = Selector::remove($item, $selector['h4']);   // 移除标题，从剩余内容提取
                            $temp = str_replace(["&nbsp;", "&amp;", "\n", "\r\n", "\r", "\0", "\t", "\x0B"], '', trim($temp));    // 不能过滤空格
                            if ($temp) {
                                $offset = strpos($temp, '<');
                                if ($offset != 0) {     // 排除0的项目
                                    $details[$h4] = $offset ? trim(substr($temp, 0, $offset)) : $temp;
                                }
                            }
                            break;
                    }
                }
            }
        }
        return $details;
    }

    /**
     * IMDb电影条目，全体工作人员解码
     * @param string $tt
     * @return array|bool
     */
    public function SubjectFullcredits(string $tt = '')
    {
        // 页面选择器
        $selector = [
            'title' => '//title',       // 网页标题栏
            'fullcredits_content' => '//div[@id="fullcredits_content"]',    // 所有演职员容器
            'dataHeader' => '//h4[contains(@class,"dataHeaderWithBorder")]',   // 演职员类型
            'h4' => '//h4',                                                    // 演职员类型
            'tr' => '//tr',          // 提取行
            'td' => '//td',          // 提取列
            'a' => '//a',
            'cast' => '//h4[@id="cast"]',       // 演职员标题
            'name' => '//td[@class="name"]',        // 名字、链接
            'credit' => '//td[@class="credit"]',    // 承担职务或岗位
        ];
        // 数据键映射关系
        $mapStructure = [
            'Directedby' => 'directors',
            'WritingCredits' => 'writers',
            //'Cast' => 'casts',    // 单独处理演职员
        ];
        // 返回的数据结构
        $rs = [
            'id' => $tt,            // 电影条目id
            // 导演 Directed by
            'directors' => [
                [
                    'id' => '',     // id
                    'alt' => '',    // url
                    'name' => '',   // 名字
                    'credit' => ''  // 承担职务或岗位
                ]
            ],
            // 编剧 Writing Credits
            'writers' => [
                [
                    'id' => '',     // id
                    'alt' => '',    // url
                    'name' => '',   // 名字
                    'credit' => ''  // 承担职务或岗位
                ]
            ],
            // 演职员 Cast
            'casts' => [
                [
                    'id' => '',     // id
                    'alt' => '',    // url
                    'name' => '',   // 名字
                    'character' => ''  // 承担职务或岗位
                ]
            ],
            '@form' => 'imdb',
            #todo... 更多
        ];
        // 读取页面缓存
        $pageCacheKey = ['Model', 'IMDbFullcredits' . 'page', $tt];
        if ($html = $this->getCache($pageCacheKey)) {
            // 存在页面缓存
        } else {
            $subjectUrl = str_replace('{id}', $tt, self::IMDbUrl['celebrity']);
            $html = $this->request(self::IMDbBase . $subjectUrl, [], false);
            /**
             * 不存在时页面特征
             * http状态码：404
             * 页面主标题：<title>404 Error - IMDb</title>
             */
            if ($html && (strpos($html, '/title/' . $tt) !== false)) {
                // 写入页面缓存(3天)
                //$this->setCache($pageCacheKey, trim($html), $this->cachePageExpire);
            } else {
                $html = false;
            }
        }
        // 初始化 不可少
        $rs = ['id' => $tt];
        if ($html) {
            // 全体工作人员
            $fullcredits_content = Selector::select($html, $selector['fullcredits_content']);
            if ($fullcredits_content) {
                $delimiter = '</table>';    // 边界上的分隔字符
                $fullcredits_item = explode($delimiter, $fullcredits_content);
                $castsSelector = 'Cast';    // 演职员特征
                foreach ($fullcredits_item as $item) {
                    $h4 = Selector::select($item, $selector['dataHeader']);
                    if ($h4) {
                        $h4 = trim(str_replace(["&nbsp;", "&amp;", "\n", "\r\n", "\r", "\0", "\t", "\x0B", ' ', " "], '', $h4));
                        $h4 = preg_replace('/\W/', '', $h4);
                    }
                    if (strpos($h4, $castsSelector) !== false) {
                        // 演职员结构
                        $Cast = [
                            'id' => '',     // id
                            'alt' => '',    // url
                            'name' => '',   // 名字
                            'character' => '',  // 饰演
                        ];
                        $casts_tr = Selector::select($item, $selector['tr']);
                        if ($casts_tr) {
                            $casts_tr = is_array($casts_tr) ? $casts_tr : array($casts_tr);
                            foreach ($casts_tr as $value) {
                                $cast_td = Selector::select($value, $selector['td']);
                                // 过滤表头空行，奇偶行才有效
                                if (is_array($cast_td) && (count($cast_td) >= 2)) {
                                    // 数据列：0.影人头像，1.影人名字，2.省略号，3.饰演
                                    // 也可以用array_shift($cast_td);移除第一列的元素再处理！
                                    if (preg_match(self::PrimaryRegex['nm'], $cast_td[1], $matches)) {
                                        $Cast['id'] = $matches[0];      // 影人id
                                        $Cast['alt'] = str_replace('{id}', $Cast['id'], self::IMDbBase . self::IMDbUrl['name']);  // 影人url
                                        $Cast['name'] = trim(Selector::select($cast_td[1], $selector['a']));    // 影人名字
                                        if (count($cast_td) === 4) {
                                            // 提取饰演
                                            $character_html = $cast_td[3];
                                            if ($character_html && is_string($character_html)) {
                                                if (strpos($character_html, '</a>')) {
                                                    $character = Selector::select($character_html, $selector['a']);
                                                    $character = is_array($character) ? $character : [$character];
                                                    array_walk($character, function (&$v, $k) {
                                                        $v = trim(str_replace(["&nbsp;", "&amp;", "\n", "\r\n", "\r", "\0", "\t", "\x0B"], '', $v), '\'');
                                                    });
                                                } else {
                                                    $character = [trim($character_html)];
                                                }
                                                $Cast['character'] = $character;
                                            }
                                        }
                                        $rs['casts'][] = $Cast;
                                    }
                                }
                            }
                        }
                    } else {
                        // 工作人员结构
                        $Crew = [
                            'id' => '',     // id
                            'alt' => '',    // url
                            'name' => '',   // 名字
                            'credit' => ''  // 承担职务或岗位
                        ];
                        $Crew_tr = Selector::select($item, $selector['tr']);
                        if ($Crew_tr) {
                            $Crew_tr = is_array($Crew_tr) ? $Crew_tr : [$Crew_tr];
                            foreach ($Crew_tr as $value) {
                                $name_td = Selector::select($value, $selector['name']);     // 示例：<a href="/name/nm1461392/?ref_=ttfc_fc_dr1"> Joachim Rønning </a>
                                $credit_td = Selector::select($value, $selector['credit']); // 示例：(written by) and
                                if (empty($name_td)) {
                                    continue;
                                }
                                if (preg_match(self::PrimaryRegex['nm'], $name_td, $matches)) {
                                    $Crew['id'] = $matches[0];      // 影人id
                                    $Crew['alt'] = str_replace('{id}', $Crew['id'], self::IMDbBase . self::IMDbUrl['name']);  // 影人url
                                    $Crew['name'] = trim(Selector::select($name_td, $selector['a']));    // 影人名字
                                    // 承担职务或岗位
                                    if ($credit_td) {
                                        $Crew['credit'] = trim($credit_td);
                                    }
                                    // 数据键映射关系
                                    $key = '';
                                    foreach ($mapStructure as $k => $v) {
                                        if (strpos($h4, $k) !== false) {
                                            $key = $v;
                                        }
                                    }
                                    $key = empty($key) ? $h4 : $key;
                                    $rs[$key][] = $Crew;
                                }
                            }
                        }
                    }
                }
            }
            return $rs;
        } else {
            return false;
        }
    }

    /**
     * IMDb发行信息解码
     * @param string $tt
     * @return mixed 失败false |成功array
     */
    public function SubjectReleaseinfo(string $tt = '')
    {
        // 页面选择器
        $selector = [
            'title' => '//title',       // 网页标题栏
            'releases' => '//h4[@id="releases"]',      // 发行日期 Release Dates
            'release-dates-table-test-only' => '//table[contains(@class,"release-dates-table-test-only")]',    // 发布日期(数据表)
            'release-date-item' => '//tr[contains(@class,"release-date-item")]',
            'release-date-item__country-name' => '//td[@class="release-date-item__country-name"]//a',
            'release-date-item__date' => '//td[@class="release-date-item__date"]',
            'release-date-item__attributes' => '//td[@class="release-date-item__attributes"]',
            'akas' => '//h4[@id="akas"]',          // 又名 Also Known As (AKA)
            'akas-table-test-only' => '//table[contains(@class,"akas-table-test-only")]',    // 又名(数据表)
            'aka-item' => '//tr[contains(@class,"aka-item")]',
            'aka-item__name' => '//td[@class="aka-item__name"]',
            'aka-item__title' => '//td[@class="aka-item__title"]',
            'tr' => '//tr',      // 提取行  可替换release-date-item和aka-item
        ];
        // 返回的数据结构
        $rs = [
            'id' => $tt,            // 电影条目id
            // 发行信息
            'release' => [
                [
                    'country' => '',        // 区域
                    'date' => '',           // 日期
                    'attributes' => '',     // 属性
                ]
            ],
            // 更详细的又名
            'akas' => [
                [
                    'country' => '',        // 区域
                    'title' => '',          // 电影名字
                ]
            ],
            'aka' => [],
            '@form' => 'imdb',
        ];
        // 读取页面缓存
        $pageCacheKey = ['Model', 'IMDbReleaseinfo' . 'page', $tt];
        if ($html = $this->getCache($pageCacheKey)) {
            // 存在页面缓存
        } else {
            $subjectUrl = str_replace('{id}', $tt, self::IMDbUrl['releaseinfo']);
            $html = $this->request(self::IMDbBase . $subjectUrl, [], false);
            /**
             * 不存在时页面特征
             * http状态码：404
             * 页面主标题：<title>404 Error - IMDb</title>
             */
            if ($html && (strpos($html, '/title/' . $tt) !== false)) {
                // 写入页面缓存(3天)
                //$this->setCache($pageCacheKey, trim($html), $this->cachePageExpire);
            } else {
                $html = false;
            }
        }
        // 初始化 不可少
        $rs['release'] = [];
        $rs['akas'] = [];
        if ($html) {
            // 发行时间
            $release_table = Selector::select($html, $selector['release-dates-table-test-only']);
            $release_item = Selector::select($release_table, $selector['release-date-item']);
            if ($release_item) {
                $release_item = is_array($release_item) ? $release_item : array($release_item);
                foreach ($release_item as $item) {
                    $release = [
                        'country' => '',        // 区域
                        'date' => '',           // 日期
                        'attributes' => '',     // 属性
                    ];
                    $release['country'] = trim(Selector::select($item, $selector['release-date-item__country-name']));
                    $release['date'] = trim(Selector::select($item, $selector['release-date-item__date']));
                    $release['attributes'] = trim(Selector::select($item, $selector['release-date-item__attributes']));
                    if (empty($release['country']) || empty($release['date'])) {
                        #todo.. 异常报错预埋点
                    }
                    $rs['release'][] = $release;
                }
            }
            // 又名
            $aka_table = Selector::select($html, $selector['akas-table-test-only']);
            $aka_item = Selector::select($aka_table, $selector['aka-item']);
            if ($aka_item) {
                $aka_item = is_array($aka_item) ? $aka_item : array($aka_item);
                foreach ($aka_item as $item) {
                    $aka = [
                        'country' => '',        // 区域
                        'title' => '',          // 电影名字
                    ];
                    $aka['country'] = trim(Selector::select($item, $selector['aka-item__name']));
                    $aka['title'] = trim(Selector::select($item, $selector['aka-item__title']));
                    if (empty($aka['country']) || empty($aka['title'])) {
                        #todo.. 异常报错预埋点
                    }
                    $rs['akas'][] = $aka;
                }
                // 算法：1.取title列；2.反转键值去重；3.提取所有键
                $aka = array_keys(array_flip(array_column($rs['akas'], 'title')));
                $rs['aka'] = $aka ? $aka : [];
            }
            return $rs;
        } else {
            return false;
        }
    }

    public function CountriesT($Country = '', $type = 'zh_cn')
    {
        $Countries = [
            'China' => '中国',
            'USA' => '美国',
            'UK' => '英国',
            'Canada' => '加拿大',
            'Finland' => '',
            'France' => '',
            'Indonesia' => '',
            'Netherlands' => '',
            'Norway' => '',
            'Philippines' => '',
            'Sweden' => '',
            'Argentina' => '',
            'Austria' => '',
            'Australia' => '',
            'Brazil' => '',
            'Chile' => '',
            'Colombia' => '',
            'Germany' => '',
            'Denmark' => '',
            'Greece' => '',
            'Hong Kong' => '香港',
            'Hungary' => '',
            'Israel' => '',
            'Italy' => '',
            'Jordan' => '',
            'South Korea' => '',
            'Kuwait' => '',
            'Kazakhstan' => '',
            'Lebanon' => '',
            'New Zealand' => '',
            'Peru' => '',
            'Portugal' => '',
            'Serbia' => '',
            'Russia' => '',
            'Saudi Arabia' => '',
            'Singapore' => '',
            'Slovakia' => '',
            'Taiwan' => '',
            'Ukraine' => '',
            'Uruguay' => '',
            'Bulgaria' => '',
            'Estonia' => '',
            'Spain' => '',
            'Ireland' => '',
            'India' => '',
            'Iceland' => '',
            'Japan' => '',
            'Latvia' => '',
            'Mexico' => '',
            'Poland' => '',
            'Romania' => '',
            'Turkey' => '',
            'Vietnam' => '',
            'Lithuania' => '',
            'Ethiopia' => '',
        ];
        return isset($Countries[$Country]) && $Countries[$Country] ? $Countries[$Country] : $Country;
    }

    public function LanguagesT($lang = '', $type = 'zh_cn')
    {
        $Languages = [
            'Mandarin' => '普通话',
            'English' => '英语',
            'Russian' => '俄语',
            'French' => '法语',
            'Japanese' => '日语',
            'Korean' => '韩语',
            'Indonesian' => '印度尼西亚语',
            'Hindi' => '印地语',
        ];
        return isset($Languages[$lang]) && $Languages[$lang] ? $Languages[$lang] : $lang;
    }

    /**
     * IMDb结构化数据内的影人信息转换成标准格式
     * @param $celebrity
     * @return array|bool
     */
    protected function formatCelebrityInJsonLD($celebrity)
    {
        if (is_array($celebrity)) {
            // 关联数组转换为索引数组
            if (isset($celebrity['@type'])) {
                $celebrity = [$celebrity];
            }
            foreach ($celebrity as $key => &$item) {
                // @type值：Person | Organization
                if (isset($item['@type']) && $item['@type']) {
                    $item['@type'] = strtolower($item['@type']);
                    switch ($item['@type']) {
                        case 'person':          // 个人
                            $k = 'nm';
                            break;
                        case 'organization':    // 团体
                            $k = 'co';
                            break;
                        default:                // 未知
                            $k = 'd';
                            break;
                    }
                    if (preg_match(self::PrimaryRegex[$k], $item['url'], $matches)) {
                        $item['id'] = $matches[0];
                        $item['alt'] = self::IMDbBase . $item['url'];
                        unset($item['url']);
                    }
                }
            }
            return $celebrity;
        }
        return false;
    }
}
