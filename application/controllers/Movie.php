<?php

use Yaf\Dispatcher;
use Movie\AbstractMovie;
use const Yaf\VERSION;

/**
 * Class MovieController
 * @see http://www.php.net/manual/en/class.yaf-controller-abstract.php
 */
class MovieController extends BaseController
{
    /**
     * 默认初始化方法，如果不需要，可以删除掉这个方法
     * 如果这个方法被定义，那么在Controller被构造以后，Yaf会调用这个方法
     */
    public function init(): void
    {
        $this->Cors();
        // 关闭自动渲染模板
        Dispatcher::getInstance()->disableView();
    }

    /**
     * 默认动作
     * Yaf支持直接把\Yaf\Request_Abstract::getParam()得到的同名参数作为Action的形参
     * @param string $name
     * @return bool
     */
    public function indexAction(string $name = 'IYUU'): bool
    {
        $file = str_replace(APPLICATION_PATH, '', __FILE__);
        $rs = [
            'name' => $name,
            'file' => $file,
            'method' => __METHOD__,
            'version' => VERSION,
        ];
        return $this->json($rs);
    }

    /**
     * 豆瓣影视条目
     * @param int $id
     * @return bool
     * @throws BadRequestException
     */
    public function subjectAction(int $id = 0): bool
    {
        $obj = AbstractMovie::getInstance('douban');
        $rs = $obj->SubjectPage($id);

        return $this->json($rs);
    }

    /**
     * 豆瓣影视条目->全部演职员
     * @param int $id
     * @return bool
     * @throws BadRequestException
     */
    public function celebritiesAction(int $id = 0): bool
    {
        $obj = AbstractMovie::getInstance('douban');
        $rs = $obj->celebritiesPage($id);

        return $this->json($rs);
    }

    /**
     * IMDb影视条目
     * @param string $tt
     * @return bool
     * @throws BadRequestException
     */
    public function titleAction(string $tt = ''): bool
    {
        $obj = AbstractMovie::getInstance('imdb');
        $rs = $obj->SubjectPage($tt);

        return $this->json($rs);
    }
}
