<?php
/**
 * 传递数据以易于阅读的样式格式化后输出
 * @param $data
 */
function P($data)
{
    $str = '<pre style="display: block;padding: 5px;margin: 2px 0 20px 0;font-size: 13px;line-height: 1.42857;color: #333;word-break: break-all;word-wrap: break-word;background-color: #F5F5F5;border: 1px solid #CCC;border-radius: 4px;">';
    if (is_bool($data)) {
        $show_data = $data ? 'true' : 'false';
    } elseif (is_null($data)) {
        $show_data = 'null';
    } else {
        $show_data = print_r($data, true);
    }
    $str .= $show_data;
    $str .= '</pre>';
    echo $str;
}

/**
 * 调试函数
 */
function P_debug()
{
    $br = PHP_SAPI === 'cli' ? PHP_EOL : '<br />';
    echo '$_GET' . $br;
    P($_GET);
    echo '$_POST' . $br;
    P($_POST);
    echo '$_REQUEST' . $br;
    P($_REQUEST);
    echo '$_COOKIE' . $br;
    P($_COOKIE);
    echo '$_SERVER' . $br;
    P($_SERVER);
}
