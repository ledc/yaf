<?php
/**
 * 配置路由协议
 */
return [
    /**
     * 影视信息路由
     */
    'douban' => [
        'type' => 'rewrite',
        'match' => '/douban/:id',
        'route' => [
            'module' => 'Index',
            'controller' => 'Movie',
            'action' => 'subject',
        ],
    ],
    'subject' => [
        'type' => 'regex',
        'match' => '#^/subject/(\d+)$#',
        'route' => [
            'module' => 'Index',
            'controller' => 'Movie',
            'action' => 'subject',
        ],
        'map' => [
            1 => 'id',
        ],
    ],
    'celebrities' => [
        'type' => 'regex',
        'match' => '#^/celebrities/(\d+)$#',
        'route' => [
            'module' => 'Index',
            'controller' => 'Movie',
            'action' => 'celebrities',
        ],
        'map' => [
            1 => 'id',
        ],
    ],
    'title' => [
        'type' => 'regex',
        'match' => '#^/title/(tt\d+)$#',
        'route' => [
            'module' => 'Index',
            'controller' => 'Movie',
            'action' => 'title',
        ],
        'map' => [
            1 => 'tt',
        ],
    ],
];
