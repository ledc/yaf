﻿## 简介

使用Yaf实现的一个影视信息查询Demo，可以查询豆瓣影视条目、豆瓣全部演职员、IMDb影视条目、IMDb全部演职员等。（Yaf是一个C语言编写的PHP框架）




## 意义

通过本Demo，可以对Yaf框架整体提高认知，熟悉以下知识点：

1. Yaf扩展安装与配置
2. php.ini配置
3. nginx重写规则
4. 目录结构（多模块）
5. Bootstrap
6. 类的加载规则（PSR4）
7. 插件的6个Hook
8. 路由和路由协议（Rewrite、Regex）
9. Dispatcher分发器
10. 控制器、请求、响应、配置等内建类
11. 异常和错误机制




## 部署方法

1. 安装Yaf扩展
2. 编辑php.ini，添加配置
3. 添加nginx伪静态
4. 完成，访问测试




## php.ini配置项

```
yaf.environ = "product"
yaf.cache_config = 1
yaf.use_namespace = On
yaf.use_spl_autoload = On
```




## nginx伪静态
```
location / {
  if (!-e $request_filename) {
    rewrite ^/(.*)  /index.php/$1 last;
  }
}
```




## 访问测试

1. 豆瓣条目：http://movie.iyuu.cn/subject/35087699
2. 豆瓣全部演职员：http://movie.iyuu.cn/celebrities/35087699
3. IMDb条目：http://movie.iyuu.cn/title/tt13696296




## 目录结构

```
+ public
  |- index.php //入口文件
  |- .htaccess //重写规则    
  |+ css
  |+ img
  |+ js
+ conf
  |- application.ini //配置文件
  |- route.php		 //路由协议
+ application
  |+ controllers
     |- Index.php //默认控制器
  |+ views    
     |+ index   //控制器
        |- index.phtml //默认视图
  |+ modules //其他模块
  |+ library //本地类库
  |+ models  //model目录
  |+ plugins //插件目录
```




## 参考

PHP官方文档：https://www.php.net/manual/zh/intro.yaf.php

鸟哥官方文档：https://www.laruence.com/manual/index.html

配置文件：https://www.laruence.com/manual/yaf.config.optional.html

注册其他命名空间：https://www.laruence.com/manual/yaf.autoloader.html

类的加载方法：https://www.laruence.com/manual/yaf.autoloader.rule.html

路由协议：https://www.laruence.com/manual/yaf.routes.static.html
